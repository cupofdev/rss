![Channel 9 RSS Logo.png](https://bitbucket.org/repo/Mxz8EM/images/3606091242-Channel%209%20RSS%20Logo.png)
# **Table of Contents** #

This document provides a table of contents for all the Channel 9 RSS Video Downloader documentation.

# **Why did I create this project ?** #
I've created this application to easily download video's from RSS feeds. 

### How do I get set up? ###

* You would need .Net 4.5 or up
* Visual Studio 2013 or up

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Contact me at mwolfaardt@gmail.com
* Visit my blog at [http://www.cupofdev.com](Link URL)