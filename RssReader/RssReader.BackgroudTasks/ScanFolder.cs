﻿namespace RssReader.BackgroudTasks
{
    using RssReader.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.IO;
    using System.Collections.Concurrent;
    

    public class ScanFolder
    {
        public async Task<List<SyndicationFeedItem>> ReadFileNamesAsync(string downloadPath)
        {
            List<SyndicationFeedItem> items = new List<SyndicationFeedItem>();

            foreach (string file in System.IO.Directory.EnumerateFiles(downloadPath, "*.mp4", System.IO.SearchOption.AllDirectories))
            {
                items.Add(new SyndicationFeedItem() { Title = file });
            }
            return items;
        }

        public static Task ReadFileNamesAsync2(string downloadPath, BlockingCollection<string> output)
        {
            return Task.Run(() =>
                {
                    foreach (string file in System.IO.Directory.EnumerateFiles(downloadPath, "*.mp4", System.IO.SearchOption.AllDirectories))
                    {
                        output.Add(file);
                    }
                    output.CompleteAdding();
                });
        }

        public static Task<bool> ScanFolderFileExistAsync(string fileName)
        {
            return Task.Run(() => System.IO.File.Exists(fileName));
        }

        public void AddToIndexFolder()
        {
           
        }
    }
}
