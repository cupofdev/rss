﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.ServiceModel.Syndication;
using System.Net;
using System.Windows.Forms;
using System.IO;
using RssReader.Model;
using System.Diagnostics;
using RssReader.Entities;

namespace RssReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            FeedSetting feedSetting = new FeedSetting();
            txtRssFeed.DataContext = feedSetting;
            btnDownloadFeed.DataContext = feedSetting;

            DriveEntity de = new DriveEntity();
            lbDrives.ItemsSource = de.GetAvailableSpace();
            lblPath.Content = @"C:\Build 2016";
        }

        private void btnDownloadFeed_Click(object sender, RoutedEventArgs e)
        {
            string url = txtRssFeed.Text;
            SyndicationFeedEntity sfe = new SyndicationFeedEntity();
            DgSyndicationFeed.ItemsSource = sfe.GetItems(url);
        }


        public async Task DownloadFile(string url, string path, IProgress<int> progress = null)
        {
            // int processCount 
            using (WebClient client = new WebClient())
            {
                client.DownloadProgressChanged += (s, e) =>
                {
                    if (progress != null)
                        progress.Report(e.ProgressPercentage);
                };

                await client.DownloadFileTaskAsync(url, path);
            }
        }

        private void btnFolder_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                lblPath.Content = fbd.SelectedPath;
            }
        }

        private async void btnClickDownloadFile_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button btn = sender as System.Windows.Controls.Button;
            btn.IsEnabled = false;
            SyndicationFeedItem context = btn.Tag as SyndicationFeedItem;
            if (context != null)
            {
                var progress = new Progress<int>(percent => { btn.Content = string.Format("{0} % done", percent); });
                if (!Directory.Exists(System.IO.Path.Combine(lblPath.Content.ToString(), Helper.FilePathParse(context.Title))))
                {
                    Directory.CreateDirectory(System.IO.Path.Combine(lblPath.Content.ToString(), Helper.FilePathParse(context.Title)));
                }
                await DownloadFile(context.VideoUrl.ToString(), System.IO.Path.Combine(lblPath.Content.ToString(), Helper.FilePathParse(context.Title), string.Concat(Helper.FilePathParse(context.Title), context.FileExt)), progress);
                CreateTextFile(context, System.IO.Path.Combine(lblPath.Content.ToString(), Helper.FilePathParse(context.Title), string.Concat(Helper.FilePathParse(context.Title), ".txt")));
            }
        }

        private void CreateTextFile(SyndicationFeedItem context, string path)
        {
            if (!File.Exists(path))
            {
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    using (StreamWriter sr = new StreamWriter(fs, Encoding.UTF8))
                    {
                        sr.WriteLine(string.Format("Title : {0}", context.Title));
                        sr.WriteLine(string.Format("Summary : {0}", context.Summary));
                        sr.WriteLine(string.Format("Url : {0}", context.VideoUrl));
                    }
                }
            }
        }

        private void hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.ToString());
        }
    }
}
