﻿using System.Collections.Generic;
using System.ComponentModel;
namespace RssReader.Model
{
    public class FeedSetting : INotifyDataErrorInfo, INotifyPropertyChanged
    {
        public FeedSetting()
        {
            Url = "http://s.ch9.ms/events/build/2015/rss/mp4high";
            IsValid = true;
        }

        private string url;
        public string Url
        {
            get
            {
                return url;
            }
            set
            {
                url = value;
                ValidateUrl(url);
            }
        }

        public bool IsValid
        {
            get
            {
                return this.isValid;
            }
            set
            {
                if (value != this.isValid)
                {
                    this.isValid = value;
                    NotifyPropertyChanged("IsValid");
                }
            }
        }
        private bool isValid;

        private void ValidateUrl(string url)
        {
            const string propertyKey = "Url";
            HashSet<string> validationUrlErrors = new HashSet<string>();

            if (string.IsNullOrEmpty(url))
            {
                validationUrlErrors.Add("RSS Feed Url can not be empty");
                validationErrors[propertyKey] = validationUrlErrors;
                IsValid = false;
                RaiseErrorsChanged(propertyKey);
            }
            if (!(url.StartsWith("http") || url.StartsWith("https")))
            {
                validationUrlErrors.Add("RSS Feed Url must start with http or https");
                validationErrors[propertyKey] = validationUrlErrors;
                IsValid = false;
                RaiseErrorsChanged(propertyKey);

            }
            else if (validationErrors.ContainsKey(propertyKey))
            {
                /* Remove all errors for this property */
                validationErrors.Remove(propertyKey);
                /* Raise event to tell WPF to execute the GetErrors method */
                IsValid = true;
                RaiseErrorsChanged(propertyKey);
            }
        }

        public event System.EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        private void RaiseErrorsChanged(string propertyName)
        {
            if (ErrorsChanged != null)
                ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName) || !validationErrors.ContainsKey(propertyName))
                return null;

            return validationErrors[propertyName];
        }

        public bool HasErrors
        {
            get { return validationErrors.Count > 0; }
        }

        private readonly Dictionary<string, ICollection<string>> validationErrors = new Dictionary<string, ICollection<string>>();

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
