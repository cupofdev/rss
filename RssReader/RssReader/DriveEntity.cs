﻿
using System.Collections.Generic;
using System.IO;
namespace RssReader
{
    public class DriveEntity
    {
        public string VolumeLabel { get; set; }
        public long TotalSize { get; set; }
        public long AvailableSize { get; set; }
        public double PercentageFree { get; set; }

        public List<DriveEntity> GetAvailableSpace()
        {
            List<DriveEntity> driveEntities = new List<DriveEntity>();
            foreach (DriveInfo driveInfo in System.IO.DriveInfo.GetDrives())
            {
                if (driveInfo.IsReady)
                {
                    DriveEntity driveEntity = new DriveEntity()
                    {
                        AvailableSize = driveInfo.AvailableFreeSpace,
                        TotalSize = driveInfo.TotalSize,
                        VolumeLabel = driveInfo.Name,
                        PercentageFree = 100 * (double)driveInfo.TotalFreeSpace / driveInfo.TotalSize
                    };
                    driveEntities.Add(driveEntity);
                }
            }
            return driveEntities;
        }
    }
}
