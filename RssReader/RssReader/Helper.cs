﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssReader
{
    public static class Helper
    {
        public static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public static double ConvertKilobytesToMegabytes(long kilobytes)
        {
            return kilobytes / 1024f;
        }

        public static string FilePathParse(string filepath)
        {
            filepath =  filepath.Replace(":", "-").Replace("?", "").Replace("/", "-").Replace("<", "").Replace("|", "").Replace('"', ' ').Replace("*", "");
            filepath = filepath.Substring(0, System.Math.Min(100, filepath.Length)).Trim();
            return filepath;
        }
    }
}
