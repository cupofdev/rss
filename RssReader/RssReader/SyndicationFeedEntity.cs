﻿namespace RssReader
{
    using RssReader.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.ServiceModel.Syndication;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;

    /// <summary>
    /// Syndication feed entity to display RSS feed data
    /// </summary>
    public class SyndicationFeedEntity
    {

        private string[] videoQuality = new string[] { High, Medium, Low };
        const string High = "high";
        const string Medium = "mid";
        const string Low = "lg";

        public List<SyndicationFeedItem> GetItems(string url)
        {
            List<SyndicationFeedItem> syndicationFeedEntities = new List<SyndicationFeedItem>();
            try
            {
                XmlReader reader = XmlReader.Create(url);
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();
                foreach (SyndicationItem item in feed.Items)
                {
                    if (!string.IsNullOrEmpty(item.Title.Text))
                    {
                        SyndicationFeedItem se = new SyndicationFeedItem();
                        se.Title = item.Title.Text;
                        se.Summary = item.Summary.Text;
                        foreach (var link in item.Links)
                        {
                            if (link.MediaType == "video/mp4")
                            {
                                se.VideoUrl = link.Uri;
                                se.FileSizeMb = Helper.ConvertBytesToMegabytes(link.Length).ToString("0.00");
                                se.FileExt = ".mp4";
                                string videoQualityResult = videoQuality.FirstOrDefault<string>(s => link.Uri.ToString().ToLower().Contains(s));
                                switch (videoQualityResult)
                                {
                                    case High:
                                        se.Quality = "High";
                                        break;
                                    case Medium:
                                        se.Quality = "Medium";
                                        break;
                                    case Low:
                                        se.Quality = "Low";
                                        break;
                                    default:
                                        se.Quality = "Unknown";
                                        break;
                                }
                                break;
                            }
                        }
                        syndicationFeedEntities.Add(se);
                    }
                }
            }
            catch (System.Net.WebException webException)
            {
                if (webException.Status == System.Net.WebExceptionStatus.ProtocolError)
                {
                    StringBuilder error = new StringBuilder();
                    error.AppendLine("Could not open the url : " + ((HttpWebResponse)webException.Response).StatusCode);
                    error.AppendLine("Description : " + ((HttpWebResponse)webException.Response).StatusDescription);
                    System.Windows.MessageBox.Show(error.ToString());
                }
            }
            return syndicationFeedEntities;
        }
    }
}
