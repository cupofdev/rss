﻿namespace RssReader.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class SyndicationFeedItem
    {

        /// <summary>
        /// Title of the feed item
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Summary of the feed item
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// Video Url of the file to download
        /// </summary>
        public Uri VideoUrl { get; set; }

        /// <summary>
        /// The file size in MB
        /// </summary>
        public string FileSizeMb { get; set; }

        /// <summary>
        /// The file extention
        /// </summary>
        public string FileExt { get; set; }

        /// <summary>
        /// The quality of the video 
        /// </summary>
        public string Quality { get; set; }

    }
}
